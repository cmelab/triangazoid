# Importing important things for our code to work correctly!
import hoomd
import hoomd.md
import json
import numpy as np
import ex_render
import matplotlib.pyplot as plt
import freud
from IPython.display import Image
import os.path
from prettytable import PrettyTable
import gsd
import gsd.pygsd
import pickle
from uncertainties import ufloat
import uncertainties.umath as umath
from trap_def import trap_types
import tri2_analysis as tri

hoomd.context.initialize("");
snapshot = hoomd.data.gsd_snapshot("trajectories/GammaTrajectory.gsd",frame=-1)
print(tri.count_triangles(snapshot));

# This will take a long time
#densities = [18,16,14,12]
#temps = [0.5,1,2,5,10]
#triangles = {}
#for kT in temps:
#    for dens in densities:
#        gsdfile = "runs/eqrun_T{}_d{}.gsd".format(kT,dens)
#        triangles[(kT,dens)] = triangles_gsd(gsdfile,step=10)

# If not been created...
#with open("eq_triangles.pkl","wb") as f:
#    pickle.dump(triangles,f)

# If has been created...
#with open("eq_triangles.pkl", 'rb') as f:
#    triangles = pickle.load(f)

# plotting the pickle data we are storing
#for kT in temps:
#    for dens in densities:
#        plt.plot(triangles[(kT,dens)][:,0], triangles[(kT,dens)][:,1]*100,
#                label="d={}".format(dens)); 
#        # 0 step, 1 %triangles
#    plt.xlabel('time step', fontsize=18);
#    plt.ylabel('percent triangle-fied', fontsize=18);
#    plt.title("T={}".format(kT),fontsize=20)
#    plt.legend()
#    plt.show()
#
#print("Trajectory for T = {} d = {}".format(kT,dens))
#Image(filename="gifs/eqrun_T{}_d{}.gif".format(kT,dens))
#
#densities = [18,16,14,12]
#temps = [0.5,1,2,5,10]
#for kT in temps:
#    for dens in densities:
#        gsdfile = "runs/eqrun_T{}_d{}.gsd".format(kT,dens)
#        rdf = get_rdf(gsdfile)
#        plt.plot(rdf.R, rdf.RDF, label="d={}".format(dens));
#
#    plt.xlabel('r', fontsize=18);
#    plt.xlim([4,15])
#    plt.ylabel('g(r)', fontsize=18);
#    plt.title("T={}".format(kT),fontsize=20)
#    plt.legend()
#    plt.show()
