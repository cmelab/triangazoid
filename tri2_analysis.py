import json
import pickle
import os.path

import numpy as np
import matplotlib.pyplot as plt
import hoomd
import hoomd.md
import freud
from IPython.display import Image
from prettytable import PrettyTable
import gsd
import gsd.pygsd
from uncertainties import ufloat
import uncertainties.umath as umath

from trap_def import trap_types
import ex_render


def autocorr1D(array):
    '''Takes in a linear numpy array, performs autocorrelation
       function and returns normalized array with half the length
       of the input'''
    ft = np.fft.rfft(array-np.average(array))
    acorr = np.fft.irfft(ft*np.conjugate(ft))/(len(array)*np.var(array))
    return acorr[0:len(acorr)//2]


def get_decorr(acorr):
    return np.argmin(acorr>0)


def error_analysis(data):
    """Returns the standard and relative error given a dataset in a 1D numpy array"""
    serr = np.std(data)/np.sqrt(len(data))
    rel_err = np.abs(100* serr/np.average(data))
    return (serr, rel_err)


def get_rdf(gsdfile,start=0,stop=None):
    """Get rdf between rigid body centers (_R0) from gsd file"""
    f = gsd.pygsd.GSDFile(open(gsdfile, 'rb'))
    t = gsd.hoomd.HOOMDTrajectory(f)
    lastframe = len(t)-1

    if start >= lastframe:
        print("""Start frame cannot be greater than the number of frames({}).
              Starting at first frame.""".format(lastframe+1))
        start = 0
    elif start < 0:
        start = 0
    if stop == None:
        stop = lastframe
    elif stop < 0:
        stop += lastframe +1
    elif stop > lastframe:
        print("""Stop frame cannot be greater than the number of frames({}).
              Stopping at last frame.""".format(lastframe+1))
        stop = lastframe

    rdf = freud.density.RDF(rmax=t[0].configuration.box[0]/4, dr=0.01);
    for frame in range(start, stop):
        # calculates rdf for trapezoids based on center (_R0)
        center_ind = t[frame].particles.types.index('_R0')
        center_pos = t[frame].particles.position[np.where(snap.particles.typeid == center_ind)]
        box = freud.box.Box(t[frame].configuration.box[0], t[frame].configuration.box[1]);
        rdf.accumulate(box, center_pos, center_pos);
    return rdf


def run_hoomd(filename, trapezoid, runTime, mixTime, mix_kT, kT, lenA, size, startgsd=None, startframe=-1):
    """
    Runs hoomd and saves output at gsd and log file

    filename: (string) base name for log and gsd files
    runTime: (int) number of run steps
    kT: (float) unitless temperature
    lenA: (float) length the 2D unit cell vector--can be used to change density
    size: (int) repeats of the unit cell in x and y
    startgsd: (string) path to gsd file to resume from gsd
    startframe: (int) frame number to use as first frame (by default the last frame)
    """

    # Initializing where our code will run
    hoomd.context.initialize("");

    # This fills in the positions of our particles to make our trapezoid
        # All positions were found using init_wrapper in trap_position_data.ipynb
        #trap_position_data.ipynb takes in a .hoomdxml which comes from running jigsawtrap.ipynb

    with open("rigid_info_jigsaw.json") as json_data:
        rigid_body_data = json.load(json_data)
    particle_positions = rigid_body_data[0]["r_positions"]

    particle_positions = np.array(particle_positions)
    # This divides the positions by 10 so they aren't so spread out
    particle_positions /= 10

    if startgsd == None:

        # Creating a unitcell with three particles as our center.
        # These are our rigid center particles.
        # The a1, a2, and a3 define our box size.
        # The mass and moment of inertia were found by running init_wrapper on the trapezoid made
        # in mbuild in a different file.
        uc = hoomd.lattice.unitcell(N = particles,
                                   a1 = [lenA, 0, 0],
                                   a2 = [0, lenA, 0],
                                   a3 = [0, 0, 0],
                                   dimensions = 2,
                                   position = [[0,0,0]],
                                   type_name = ['_R0'],
                                   mass = [45.0],
                                   moment_inertia = [[65.83333333, 322.5, 388.33333333]],
                                   orientation = [[1,0,0,0]])

        # This replicates the rigid center particle 'size' in the x direction and 'size' in the y direction
        system = hoomd.init.create_lattice(unitcell=uc, n = [size,size])

        # Adding the types of particles that we want.
        # Z is normal particles
        # A,B,C,D,E,F are our sticky particles
        system.particles.types.add('Z')
        system.particles.types.add('A')
        system.particles.types.add('B')
        system.particles.types.add('C') #Needed for certain trapezoids with only one patch type

    else:
        system = hoomd.init.read_gsd(startgsd,frame=startframe)

    # Creates the rigid body but nothing is in it yet
    rigid = hoomd.md.constrain.rigid()

    # Puts the particles in their correct positions based on their types
    rigid.set_param('_R0', types=trap_types[trapezoid],positions=particle_positions)

    rigid.create_bodies()

    # Identifying our neighborlist
    nl = hoomd.md.nlist.cell()

    # Lennard Jones potential is specified with the desired r_cut value
    lj = hoomd.md.pair.lj(r_cut=2.5, nlist=nl)

    # Helps not cut the potential off so abruptly
    lj.set_params(mode='xplor')

    # Sets the interactions between the different groups of particles.

    # Interations dealing with just rigid centers
    lj.pair_coeff.set('_R0', '_R0', epsilon=0, sigma=0, r_cut=False)

    # Interactions dealing with particle A
    lj.pair_coeff.set('A', 'A', epsilon=0, sigma=0, alpha=0)
    lj.pair_coeff.set('A', '_R0', epsilon=0, sigma=0)
    lj.pair_coeff.set('A', 'B', epsilon=eps_sticky, sigma=sig_sticky)
    lj.pair_coeff.set('A', 'C', epsilon=0, sigma=0, alpha=0)
    lj.pair_coeff.set('A', 'Z', epsilon=2, sigma=0.9, alpha=0)

    # alpha = 0 means the potential is purely repulsive

    # Interactions dealing with particle B
    lj.pair_coeff.set('B', 'B', epsilon=0, sigma=0, alpha=0)
    lj.pair_coeff.set('B', 'C', epsilon=0, sigma=0, alpha=0)
    lj.pair_coeff.set('B', '_R0', epsilon=0, sigma=0)
    lj.pair_coeff.set('B', 'Z', epsilon=2, sigma=0.9, alpha=0)

    # Interactions dealing with particle C
    lj.pair_coeff.set('C', 'C', epsilon=eps_sticky, sigma=sig_sticky)
    lj.pair_coeff.set('C', '_R0', epsilon=0, sigma=0)
    lj.pair_coeff.set('C', 'Z', epsilon=2, sigma=0.9, alpha=0)

    # Interactions dealing with particle Z
    lj.pair_coeff.set('Z', 'Z', epsilon=2, sigma=0.9, alpha=0)
    lj.pair_coeff.set('Z', '_R0', epsilon=0, sigma=0)

    hoomd.md.integrate.mode_standard(dt=dt);

    # Creates the rigid_center group and applies langevin forces to them
    rigid_center = hoomd.group.rigid_center()
    integrate = hoomd.md.integrate.langevin(group=rigid_center, kT=mix_kT, seed=42);

    # Make "runs" directory if it does not exist
    outfile = "runs"
    if not os.path.exists(outfile):
        os.makedirs(outfile)

    # Writes our results into a .log file
    hoomd.analyze.log(filename=outfile+"/{}.log".format(filename),
                     quantities=['potential_energy',
                                'translational_kinetic_energy',
                                'rotational_kinetic_energy'],
                     period=100,
                     overwrite=True);

    # Writes the simulation into a .gsd file
    hoomd.dump.gsd(outfile+"/{}.gsd".format(filename),
                  period=gsd_period,
                  group=hoomd.group.all(),
                  overwrite=True);

    # Let's run it!
    hoomd.run(mixTime)
    integrate.set_params(kT=kT)
    hoomd.run(runTime)


def get_neighbors(box,positions,rcut=1):
    """returns a list of lists of neighboring particles within distance rcut
    accounting for pbc in given simulation box."""

    lc = freud.locality.LinkCell(box,rcut)
    lc.compute(box,positions)
    neighborlist=[]
    for i in range(positions.shape[0]):
        # Cell containing particle i
        cell = lc.getCell(positions[i])
        # List of cell's neighboring cells
        cellNeighbors = lc.getCellNeighbors(cell)
        # Iterate over neighboring cells (including our own)
        i_neighbors = []
        for neighborCell in cellNeighbors:
            # Iterate over particles in each neighboring cell
            for neighbor in lc.itercell(neighborCell):
                i_neighbors.append(neighbor)

        neighborlist.append(i_neighbors)
    # wonky (but functional) way of getting no repeats
    uniq_neighbors = [list(x) for x in set(tuple(x) for x in neighborlist)]

    return uniq_neighbors


def get_box(snap):
    """Returns a hoomd.data.boxdim object given a snapshot regardless of snapshot origin.
    There is a difference in how snapshots generated with gsd.hoomd.HOOMDTrajectory
    and hoomd.data.gsd_snapshot deal with the box: one returns a boxdim object, the other a
    numpy array. By using this function, they both return boxdim objects."""

    if isinstance(snap,gsd.hoomd.Snapshot):
        box2 = snap.configuration.box
        box = hoomd.data.boxdim()
        box.Lx = box2[0]
        box.Ly = box2[1]
        box.Lz = box2[2]
        box.dimensions = 2

    elif isinstance(snap,hoomd._hoomd.SnapshotSystemData_float):
        box = snap.box

    return box


def count_triangles(snapshot, d_cluster=2):
    """Returns the number of triangles given one snapshot frame."""
    
    particle_ids = np.where(snapshot.particles.typeid==snapshot.particles.types.index('_R0'))[0]
    ntraps = len(particle_ids)
    # Get the snapshot of the simulation I want
    box = get_box(snapshot)
    # Find the positions of the specific particle that I want
    positions = snapshot.particles.position
    left_positions = positions[ntraps+37::45]
    # Create a list of where there are groups of the specific particle close together
    neighborlist = get_neighbors(box,left_positions,d_cluster)

    # Should create a list of groups where 3 trapezoids were near enough to each other (aka made a triangle)
    # This cuts down on groups of 2 or 4 who happen to have the top left particle within the r_cut range
    triangles = len([group for group in neighborlist if len(group) == 3])
    print([group for group in neighborlist if len(group) ==3])
    
    return triangles


def triangles_gsd(gsdfile,start=0,stop=None,step=1):
    """Returns np.array with frame number and percent triangle-fied for startframe
    to stopframe in gsd file.
    This is SLOW; you can up the step size to save time."""

    f = gsd.pygsd.GSDFile(open(gsdfile, 'rb'))
    t = gsd.hoomd.HOOMDTrajectory(f)
    lastframe = len(t)-1

    if start >= lastframe:
        print("""Start frame cannot be greater than the number of frames({}).
              Starting at first frame.""".format(lastframe+1))
        start = 0
    elif start < 0:
        start = 0

    if stop == None:
        stop = lastframe

    elif stop > lastframe:
        print("""Stop frame cannot be greater than the number of frames({}).
              Stopping at last frame.""".format(lastframe+1))
        stop = lastframe

    n_triangles = []
    # assumes number of trapezoids is constant
    n_trapezoid = len(np.where(t[0].particles.typeid == t[0].particles.types.index('_R0'))[0])

    for frame,snap in enumerate(t[start:stop:step]):

        n_triangles.append([frame*step,count_triangles(snap)*3/n_trapezoid])

    return np.vstack(n_triangles)
